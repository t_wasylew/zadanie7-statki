package statki;

import java.util.Scanner;

public class Field {

    private int size;
    private String[][] field;
    Scanner scanner = new Scanner(System.in);

    public Field(int size) {
        this.size = size;
        field = new String[size][size];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = "O ";
            }
        }
    }

    /**
     * Method to locate ships on the Field
     */
    public void settingShips() {
        int counter = 1;
        do {
            System.out.printf("Podaj wspolrzedne dla %d statka: ", counter);
            String wspolrzedne = scanner.nextLine();
            String[] strings = wspolrzedne.trim().split(" ");
            try {
                int x = Integer.parseInt(strings[0]);
                int y = Integer.parseInt(strings[1]);
                if (field[x][y].equals("S ")) {
                    System.out.println("W tym miejscu znajduje się juz statek. Podaj nowe wspolrzedne");
                } else {
                    field[x][y] = "S ";
                    counter++;
                }
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.out.println("Wspolrzedne poza mapa");
            } catch (NumberFormatException nfe) {
                System.out.println("Nie wpisałeś nic!");
            }
        } while (counter != 6);
        System.out.println("Twoja mapa");
        printField();
    }

    /**
     * Method to print Field
     */
    public void printField() {
        for (int i = 0; i < field.length; i++) {
            System.out.println();
            for (int j = 0; j < field.length; j++) {
                System.out.print(field[i][j]);
            }
        }
        System.out.println();
    }

    /**
     * Method to check cells in field
     * @param x X-axis
     * @param y Y-axis
     * @return Result of shooting
     */
    public int checkCell(int x, int y) {
        try {
            if (y <= size && x <= size) {
                if (field[x][y].equals("O ")) {
                    System.out.println("Sprawdzam!");
                    field[x][y] = "X ";
                    return 0;
                } else if (field[x][y].equals("S ")) {
                    System.out.println("Trafiony zatopiony!");
                    field[x][y] = "Z ";
                    return 1;
                } else {
                    System.out.println("Miejsce juz bylo sprawdzane!");
                    return -1;
                }
            }
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            System.out.println("Wspolrzedne poza mapa");
            return -1;
        }
        return -1;
    }
}
