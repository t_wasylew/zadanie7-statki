package statki;

import java.util.Scanner;

public class Gra {

    public void run() {

        int size = getSize();
        Player player1 = new Player(new Field(size));
        Player player2 = new Player(new Field(size));
        preparingField(player1, player2);

            do {
            System.out.println("Gracz 1");
            player1.turn(player2);
            if (player1.checkScore() == 5) {
                break;
            }
            System.out.println("Gracz 2");
            player2.turn(player1);
            if (player2.checkScore() == 5) {
                break;
            }
        } while (player1.getScore() != 5 || player2.getScore() != 5);
    }

    /**
     * Preparing battle field by setting ships
     * @param player1 Player 1
     * @param player2 Player 2
     */
    private void preparingField(Player player1, Player player2) {
        System.out.println("Gracz 1 rozstawia statki");
        player1.field.settingShips();
        System.out.println("Gracz 2 rozstawia statki");
        player2.field.settingShips();
    }

    /**
     * Getting size of field with Scanner
     * @return size of field
     */
    private int getSize() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wielkosc planszy");
        String fieldSize = scanner.nextLine();
        return Integer.parseInt(fieldSize);
    }
}



