package statki;

import java.util.Scanner;

public class Player {

    Field field;
    private int score;
    Scanner scanner = new Scanner(System.in);

    public Player(Field field) {
        this.field = field;
        score = 0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Checking current player score. If current score equals 5, game is won and finished
     * @return current score
     */
    public int checkScore() {
        if (getScore() == 5) {
            System.out.println("Brawo wygrales!");
            return getScore();
        }else {
            System.out.printf("Masz: %d \n", getScore());
            return 0;
        }
    }


    /**
     * Shooting into opposite player field
     * @param player Player which field is used
     */
    public void turn(Player player) {
        System.out.println("Podaj wspolrzedne x i y lub wpisz quit aby zakonczyc: ");
        String stringNumber = scanner.nextLine();
        if (stringNumber.equals("quit")) {
            System.out.println("Quitting");
           return;
        }
        String[] string = stringNumber.trim().split(" ");
        int x = Integer.parseInt(string[0]);
        int y = Integer.parseInt(string[1]);
       do {
           if (player.field.checkCell(x, y) == 1) {
               addScore();
           }
       } while (player.field.checkCell(x,y) == -1);
    }

    /**
     * Adding point to score if player destroyed ship
     */
    private void addScore() {
        setScore(getScore()+1);
    }
}